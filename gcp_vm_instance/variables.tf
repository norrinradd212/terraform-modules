variable "region" {
  type    = string
}

variable "image" {
  type    = string
  default = "ubuntu-os-cloud/ubuntu-1804-lts"
}


variable "machine_type" {
  type    = string
  default = "e2-micro"
}

variable "subnetCIDRblock" {
  default = "10.0.0.0/16"
}

variable "CIDRblock" {
  default = "0.0.0.0/0"
}

variable "projectName" {
  type    = string
  default = "default"
}

variable "ssh-key" {
  type    = string
  default = ""
}

variable "startup-script" {
  type    = string
  default = ""
}