terraform {
  required_version = ">= 0.12"
}


resource "google_compute_instance" "instance" {
  name         = "${var.projectName}-instance"
  machine_type = var.machine_type
  boot_disk {
    initialize_params {
      image = var.image
    }
  }
  network_interface {
    subnetwork = google_compute_subnetwork.network_subnetwork.name
    access_config {

    }
  }
  metadata = {
    ssh-keys = "${file("${var.ssh-key}")}"
  }
  metadata_startup_script = file("${var.startup-script}")

}

#-----------------------------------------------------------

resource "google_compute_network" "network" {
  name = "${var.projectName}-network"
}
#-----------------------------------------------------------

resource "google_compute_subnetwork" "network_subnetwork" {
  name          = "${var.projectName}-subnet"
  region        = var.region
  network       = google_compute_network.network.self_link
  ip_cidr_range = var.subnetCIDRblock
}