terraform {
  required_version = ">= 0.12"
}

resource "aws_instance" "instance" {
  count                       = var.enabled ? 1 : 0
  ami                         = var.ami
  instance_type               = var.instance_type
  subnet_id                   = aws_subnet.subnet.id
  vpc_security_group_ids      = var.vpc_security_group_ids
  key_name                    = var.key_name
  associate_public_ip_address = var.associate_public_ip_address
  user_data                   = var.user_data == "" ? null : var.user_data
  tags = {
    Name = "${var.projectName}_server"
  }
}
resource "aws_subnet" "subnet" {
  vpc_id     = var.vpc_id
  cidr_block = var.subnetCIDRblock
  tags = {
    Name = "${var.projectName}_subnet"
  }
}