variable "key_name" {
  type = string
}

variable "enabled" {
  default = true
}

variable "ami" {
  type    = string
  default = "ami-0d527b8c289b4af7f"
}

variable "user_data" {
  default = ""
  type    = string
}

variable "vpc_id" {
  type = string
}

variable "instance_type" {
  type    = string
  default = "t2.micro"
}

variable "subnetCIDRblock" {
  type = string
}

variable "projectName" {
  type    = string
  default = "Default"
}

variable "vpc_security_group_ids" {
}

variable "associate_public_ip_address" {
  default = true
  type    = bool

}