output "instance_subnet" {
  value = aws_subnet.subnet
}

output "webinstance" {
  value = aws_instance.instance
}
